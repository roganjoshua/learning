= Organisation

.Documentation should be structured and reusable
Asciidoctor files can include other files, including your source code, other asciidoctor files.

[source, asciidoc]
----
book.adoc
    └── images
        └── figure1.png
    └── chapters
        ├── chapter1.adoc
        ├── chapter2.adoc
    └── examples
        ├── library.js
        ├── helloworld.js
----
