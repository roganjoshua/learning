= Vim Primer

****
* vim is wtf, really!
* vim is ffs, omg.
* vim is about the keyboard.
* vim is about not losing context.
* vim is zen.
****

I believe people should use vim for the following three reasons:

* It's ubiquitous.
You don't have to worry about learning a new editor on various boxes.
* It's scalable.
You can use it just to edit config files or it can become your entire writing and coding platform.
* It's powerful.
Because it works like a language Vim takes you from frustrated to demigod very quickly.

You should consider competence with Vim the way you consider competence with your native language, or basic maths, etc.

So much in technology starts with being good with your editor.


== Getting Started
vim, at first, seems a very painful way to edit files.
At first, you cannot even type and enter text.

But vim is about *modes*.

vim believes that, normally, there is structure to the document you are writing.

Often this is code, the structure means that navigation is important.

vim starts in _Normal_ mode. 

=== Navigation Mode
Navigation mode doesn't let type text, it is just for moving around.

Useful tips

* vimgrep ignore .gitignore files

[source, vim]
----
 :noautocmd vimgrep /{pattern}/gj `git ls-files`
----


