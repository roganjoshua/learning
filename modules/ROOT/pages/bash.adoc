= Logging in

Depending on how you log in and using bash various files get called.

Non-interactive logins use .rc files like .bashrc

 +----------------+-----------+-----------+------+
 |                |Interactive|Interactive|Script|
 |                |login      |non-login  |      |
 +----------------+-----------+-----------+------+
 |/etc/profile    |   A       |           |      |
 +----------------+-----------+-----------+------+
 |/etc/bash.bashrc|           |    A      |      |
 +----------------+-----------+-----------+------+
 |~/.bashrc       |           |    B      |      |
 +----------------+-----------+-----------+------+
 |~/.bash_profile |   B1      |           |      |
 +----------------+-----------+-----------+------+
 |~/.bash_login   |   B2      |           |      |
 +----------------+-----------+-----------+------+
 |~/.profile      |   B3      |           |      |
 +----------------+-----------+-----------+------+
 |BASH_ENV        |           |           |  A   |
 +----------------+-----------+-----------+------+
 |                |           |           |      |
 +----------------+-----------+-----------+------+
 |                |           |           |      |
 +----------------+-----------+-----------+------+
 |~/.bash_logout  |    C      |           |      |
 +----------------+-----------+-----------+------+

.Credit to http://www.solipsys.co.uk/new/BashInitialisationFiles.html
image::BashStartupFiles1.png[Bash Start up]
